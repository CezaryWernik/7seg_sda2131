#define DATA 8
#define CLK 9
#define ENABLE 7
#define BLANK 10

const unsigned char DIGITS[] = {
  0xFC, //0
  0x60, //1
  0xDA, //2
  0xF2, //3
  0x66, //4
  0xB6, //5
  0xBE, //6
  0xE0, //7
  0xFE, //8
  0xF6  //9
  };
const unsigned char DOT = 0x01;

String inputString = "";
boolean stringComplete = false;

void setup() {
  Serial.begin(9600);
  pinMode(DATA, OUTPUT);
  pinMode(CLK, OUTPUT);
  pinMode(ENABLE, OUTPUT);
  pinMode(BLANK, OUTPUT);
  inputString.reserve(200);
}

int d1=0;
int d2=0;
void loop() {
  unsigned char T1 = DIGITS[d1];
  unsigned char T2 = DIGITS[d2];
  digitalWrite(ENABLE,HIGH);
  unsigned char BIT_INDEX=0x80;
  for(int i=0;i<16;i++){
    if(i<8){
      digitalWrite(DATA,((T1&BIT_INDEX)==0x00)?(LOW):(HIGH));
      Serial.write("T1:");
      Serial.println((bool)(T1&BIT_INDEX));
    }else{
      digitalWrite(DATA,((T2&BIT_INDEX)==0x00)?(LOW):(HIGH));
      Serial.write("T2:");
      Serial.println((int)(T2&BIT_INDEX));
    }
    
    _clk();
    BIT_INDEX = BIT_INDEX>>1;
    if(BIT_INDEX==0x00)
      BIT_INDEX=0x80;
    Serial.write("IN:");
    Serial.println((int)(BIT_INDEX));
  }
  digitalWrite(ENABLE,LOW);
  digitalWrite(DATA,LOW);
  
  d2++;
  if(d2>9){
    d2=0;
    d1++;
    }
  if(d1>9)
    d1=0;
  
  delay(1000);
  /*serialEvent();
  if (stringComplete) {
    Serial.println("SELECT: "+inputString);
    if(inputString.substring(0,1) == "D"){
      if(inputString.substring(2,3) == "0"){
        digitalWrite(DATA,LOW);
        }else{
          digitalWrite(DATA,HIGH);
        }
    }
    if(inputString.substring(0,1) == "C"){
      if(inputString.substring(2,3) == "0"){
        digitalWrite(CLK,LOW);
        }else{
          digitalWrite(CLK,HIGH);
        }
    }
    if(inputString.substring(0,1) == "E"){
      if(inputString.substring(2,3) == "0"){
        digitalWrite(ENABLE,LOW);
        }else{
          digitalWrite(ENABLE,HIGH);
        }
    }
    if(inputString.substring(0,1) == "B"){
      if(inputString.substring(2,3) == "0"){
        digitalWrite(BLANK,LOW);
        }else{
          digitalWrite(BLANK,HIGH);
        }
    }
    inputString = "";
    stringComplete = false;
  }*/
  /*
  digitalWrite(ENABLE,HIGH);
  //1
  digitalWrite(DATA,LOW);_clk();
  //2
  digitalWrite(DATA,HIGH);_clk();
  //3
  digitalWrite(DATA,LOW);_clk();
  //4
  digitalWrite(DATA,HIGH);_clk();
  //5
  digitalWrite(DATA,LOW);_clk();
  //6
  digitalWrite(DATA,HIGH);_clk();
  //7
  digitalWrite(DATA,LOW);_clk();
  //8
  digitalWrite(DATA,HIGH);_clk();
  //9
  digitalWrite(DATA,LOW);_clk();
  //10
  digitalWrite(DATA,HIGH);_clk();
  //11
  digitalWrite(DATA,LOW);_clk();
  //12
  digitalWrite(DATA,HIGH);_clk();
  //13
  digitalWrite(DATA,LOW);_clk();
  //14
  digitalWrite(DATA,HIGH);_clk();
  //15
  digitalWrite(DATA,LOW);_clk();
  //16
  digitalWrite(DATA,HIGH);_clk();
  digitalWrite(ENABLE,LOW);

  
  digitalWrite(DATA,LOW);
  delay(1000);
  
  digitalWrite(ENABLE,HIGH);
  //2
  digitalWrite(DATA,HIGH);_clk();
  //3
  digitalWrite(DATA,LOW);_clk();
  //4
  digitalWrite(DATA,HIGH);_clk();
  //5
  digitalWrite(DATA,LOW);_clk();
  //6
  digitalWrite(DATA,HIGH);_clk();
  //7
  digitalWrite(DATA,LOW);_clk();
  //8
  digitalWrite(DATA,HIGH);_clk();
  //9
  digitalWrite(DATA,LOW);_clk();
  //10
  digitalWrite(DATA,HIGH);_clk();
  //11
  digitalWrite(DATA,LOW);_clk();
  //12
  digitalWrite(DATA,HIGH);_clk();
  //13
  digitalWrite(DATA,LOW);_clk();
  //14
  digitalWrite(DATA,HIGH);_clk();
  //15
  digitalWrite(DATA,LOW);_clk();
  //16
  digitalWrite(DATA,HIGH);_clk();
  //1
  digitalWrite(DATA,LOW);_clk();
  digitalWrite(ENABLE,LOW);

  
  digitalWrite(DATA,LOW);
  delay(1000);
  */
}
void _clk(){
  digitalWrite(CLK,HIGH);
  //delay(10);
  digitalWrite(CLK,LOW);
  //delay(10);
  }

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
